%% Set manipulator link parameters
L1 = Link('d', 0.125, 'a', 0, 'alpha', pi/2);
L2 = Link('d', 0, 'a', 0.210, 'alpha', 0);
L3 = Link('d', 0, 'a', -0.075, 'alpha', -pi/2);
L4 = Link('d', 0.210, 'a', 0, 'alpha', pi/2);
L5 = Link('d', 0, 'a', 0, 'alpha', -pi/2);
L6 = Link('d', 0.070, 'a', 0, 'alpha', 0);
bot = SerialLink([L1 L2 L3 L4 L5 L6], 'name', 'tENSO');

%% Choose a set of angles
q = [0, 0, 2*pi, 0, 0, 0];
theta = q + [0, 0, -pi, 0, 0, 0]; % set angle offsets
%%
% Velocidade do movimento
t = [0:0.05:2]';

% Posi��o final e inicial
t1 = transl(0.2850, 0, 0.1550);
t2 = transl(0.2, 0.1, 0.1550);

%
Ts = ctraj(t1, t2, length(t));
qc = bot.ikine6s(Ts);
bot.plot(qc);
%% Show the robot
%figure(1); clf
%bot.plot(theta);
%view(150,10);