%% Global Parameters
linkSize = [210, 210];
startingAngles = [0, pi/2, -pi/2];
destination = [0, 0, 420];
step = 0.1;

%% QLearning
epoch = 1000;
maxRoute = 500;

qlearning = QLearning(startingAngles, linkSize, step);
route = qlearning.train(destination, epoch, maxRoute);

[rows, ~] = size(route);
coord = zeros(rows, 3);
for k = 1:rows
    coord(k, 1:3) = cartesian(route(k, :), linkSize);
end

x = coord(:,1);
y = coord(:,2);
z = coord(:,3);
figure(1);
% Iterate over path
for i = 1:numel(x)
    
    q0 = my_denso_invkin(x(i), y(i), z(i));

    q = [...
        q0(1), ...
        q0(2), ...
        q0(3)-pi/2, ...
        0, ...
        - ( pi/2 + q0(2) + q0(3) ), ...
        0];
    cla;
    plotDENSOframes(q);
    view(130, 30);
    axis tight
    pause(.01);
    
end
hold on
plot3(x,y,z)
rotate3d on
viewRoute(coord, destination);

%% A*
route = traceRoute(startingAngles, destination, step, linkSize);

[rows, ~] = size(route);
coord = zeros(rows, 3);
for k = 1:rows
    coord(k, 1:3) = cartesian(route(k, :), linkSize);
end

x = coord(:,1);
y = coord(:,2);
z = coord(:,3);

figure(2);
% Iterate over path
for i = 1:numel(x)
    
    q0 = my_denso_invkin(x(i), y(i), z(i));

    q = [...
        q0(1), ...
        q0(2), ...
        q0(3)-pi/2, ...
        0, ...
        - ( pi/2 + q0(2) + q0(3) ), ...
        0];
    cla;
    plotDENSOframes(q);
    view(130, 30);
    axis tight
    pause(.01);
    
end
hold on
plot3(x,y,z)
rotate3d on

viewRoute(coord, destination);
