clf

epoch = 40;
counter = 0;
p = zeros (epoch, 2);

while epoch > counter
    r = 100*rand();
    p (counter + 1, :) = [r; 0];
    
    if (r > 75)
        p (counter + 1, 2) = 5;
    end
    counter = counter + 1;
end

p = flipud (p);

for ii = 1:length(p)
    if p(ii,2)<1
          c = '-bo';
      else
          c = '-ro';
    end
      plot (ii, p (ii), c);
      hold on
end

plot (p (:, 1));
xlabel('Epoch');
ylabel('R');