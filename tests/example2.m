%% Set manipulator link parameters
L1 = Link('d', 0.125, 'a', 0, 'alpha', pi/2);
L2 = Link('d', 0, 'a', 0.210, 'alpha', 0);
L3 = Link('d', 0, 'a', -0.075, 'alpha', -pi/2);
L4 = Link('d', 0.210, 'a', 0, 'alpha', pi/2);
L5 = Link('d', 0, 'a', 0, 'alpha', -pi/2);
L6 = Link('d', 0.070, 'a', 0, 'alpha', 0);
bot = SerialLink([L1 L2 L3 L4 L5 L6], 'name', 'tENSO');

%% Choose a set of angles
q = [0, 0, 2*pi, 0, 0, 0];
theta = q + [0, 0, -pi, 0, 0, 0]; % set angle offsets
%%
% Velocidade do movimento
t = [0:0.05:2]';

% Posi��o final e inicial
t1 = transl(0.2850, 0, 0.1550);
%t2 = transl(0.2, 0.49, 0.5);
% Desired coordinates
destination = [0 0 620];
% Step attribute to modify angles
step = 0.1;
% Arbitrary sizes for arm's components
linkSize = [410 210];
% Starting position
startingAngles = [0 pi/2 -pi/2];

route = traceRoute(startingAngles, destination, step, linkSize);

q = [route(1, :), 0, 0, 0];
t = bot.fkine(q);
qc = bot.ikine6s(t);
for k = 2:(length(route)-1)
    q = [route(k, :), 0, 0, 0];
    q2 = [route(k+1, :), 0, 0, 0];

    t3 = bot.fkine(q);
    t4 = bot.fkine(q2);
    
    Tf = ctraj(t3, t4, length(t));
    
    
    qc = [qc; bot.ikine6s(Tf)];
end

%
bot.plot(qc);