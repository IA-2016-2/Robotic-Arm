function densoAnimation(route)

% par�metros do bra�o
L1 = Link('d', 0.125, 'a', 0, 'alpha', pi/2);
L2 = Link('d', 0, 'a', 0.210, 'alpha', 0);
L3 = Link('d', 0, 'a', -0.075, 'alpha', -pi/2);
L4 = Link('d', 0.210, 'a', 0, 'alpha', pi/2);
L5 = Link('d', 0, 'a', 0, 'alpha', -pi/2);
L6 = Link('d', 0.070, 'a', 0, 'alpha', 0);
bot = SerialLink([L1 L2 L3 L4 L5 L6], 'name', 'DENSO');

% ajustando o tamanho da matriz de �ngulos
linhas = size(route, 1);
df6 = zeros (linhas, 6);
df6 (1:linhas, 1:3) = route (1:linhas, 1:3);

% animando
bot.plot(df6)

end