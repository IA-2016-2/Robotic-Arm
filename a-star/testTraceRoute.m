% Desired coordinates
destination = [210 120 410];

% Step attribute to modify angles
step = 0.1;

% Arbitrary sizes for arm's components
linkSize = [410 210];

% Starting position
startingAngles = [0 pi/2 -pi/2];

route = traceRoute(startingAngles, destination, step, linkSize);
croute = zeros (length(route),3);
%To test if the angles are consistents
disp('       Angles        =>        Coord');
for k = 1:length(route)
    coord = cartesian(route(k, :), linkSize);
    croute(k,1:3) = coord (:);
    disp(['(', num2str(route(k, 1)), ', ', num2str(route(k, 2)), ', ', num2str(route(k, 3)), ')', ...
        ' | ', ...
        '(', num2str(coord(1)), ', ', num2str(coord(2)), ', ', num2str(coord(3)), ')']);
end

viewRoute (croute,destination);
