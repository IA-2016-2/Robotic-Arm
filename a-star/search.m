%% Builds a tree of nodes where the root node is the start and the 
%  returning node is the destination.
%  SYNTAX: search(start, destination, step, linkSize, tolerance, frontier)
%  WHERE: Start is the root node using the Node object. Destination is the 
%  1x3 cartesian coordinates representing the target destination. Step is 
%  the atribute to build new successors, its the parameter that makes the 
%  joint angles change. LinkSize is a 1x2 matrix that represents the sizes
%  of each arm component. Tolerance is optional, it defines how far a 
%  given node can be from the desired destination to become the result, 
%  setting it 0 is dangerous because it can end in an infinite loop. 
%  Frontier is optional, used only internally for recursion.
%  OUTPUT: The node representing the destination. Recursively calling its 
%  parents you can see the best route.

function node = search(start, destination, step, linkSize, tolerance, frontier)
    
    if ~exist('frontier', 'var')
        frontier = start;
    end
    
    if ~exist('tolerance', 'var')
        %TODO Set default tolerance  based on step
        tolerance = 12;
    end
    
    edist = calculateDistance (cartesian(start.theta, linkSize),destination);
    
    if edist < tolerance
        node = start;
    else
        % Removing start from frontier
        for k = 1:length(frontier)
            if frontier(k) == start
                frontier(k) = [];
                break
            end
        end
     
        frontier = [frontier, start.buildCandidates(step)];
        
        % Choosing successor
        %min = Inf;
        next = bestCandidate (frontier,destination,linkSize);
        next = frontier(next);
        if (length (frontier) > 1080)
            frontier = [];
        end
        %for k = 1:length(frontier)
        %    candidate = cartesian(frontier(k).theta, linkSize);
        %    distance = calculateDistance(candidate, destination);
        %    f = distance + frontier(k).cost;
        %    if f < min
        %        min = f;
        %        next = k;
        %    end
        %end
        
        % Plot successor (for debugging)
        % disp(cartesian(frontier(next).theta, linkSize));
        
        % Taking the best step for recursion
        %node = search(frontier(next), destination, step, linkSize, tolerance, frontier);         
        node = search(next, destination, step, linkSize, tolerance, frontier);         
    end
end

function BestCandidate = bestCandidate(vcandidate,dest, linkSize)
% Best candidate will be the closest to dest and with smallest variation
    smv = [Inf Inf Inf]; % smallest variation
    smc = Inf; % smallest cost
    BestCandidate = 0;
    for i = 1:length(vcandidate)
        cartOfCandidate = cartesian(vcandidate(i).theta,linkSize);
        dn = dest - cartOfCandidate;
        for j = 1:length(dn)
            if dn(j) < 0
                dn(j) = dn(j) * -1;
            end
        end
        vdist = calculateDistance(cartOfCandidate,dest);
        finalcost = vcandidate(i).cost + vdist;
        %if (dn(1) < smv(1) && dn(2) < smv(2) && dn(3) < smv(3))...
        %        && finalcost < (smc)
        if finalcost < smc
            BestCandidate = i;
            smc = finalcost;
            smv = dn;
        end
    end
end