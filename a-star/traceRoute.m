%% This function runs A* to find the route from a set of angles to a destination.
%  SYNTAX: traceRoute(startingAngles, destination, step, linkSize)
%  WHERE: startingAngles is a 1x3 matrix containing the three angles from
%  the initial state. destination is a 1x3 matrix with the cartesian
%  coordinates to the target position. step is the atribute to build new 
%  successors for a given node, its the parameter that sets how much the 
%  joint angles will change in each action. linkSize is a 1x2 matrix that 
%  represents the sizes of each arm component.
%  OUTPUT: A nx3 matrix, where n is the number of the steps to finish the
%  movement.
function route = traceRoute(startingAngles, destination, step, linkSize)

    % Using A* to build route tree
    start = Node(startingAngles);
    node = search(start, destination, step, linkSize);

    % The node instance returned by search algorithm is the destination node
    % where we will use to loop through the parents recursively
    current = node;
    route = [];

    while ~isequal(current.parent, [])
        route = [route; current.theta(1), current.theta(2), current.theta(3)];
        current = current.parent;
    end
    route = [route; start.theta(1), start.theta(2), start.theta(3)];

    % Before, the array was upside down, we solve it with flipup
    route = flipud(route);
end