classdef Node < handle
    properties
        theta
        cost
        parent
    end
    methods
        function obj = Node(theta, parent)
            if ~exist('parent', 'var')
                obj.parent = [];
                obj.cost = 1;
            else
                obj.parent = parent;
                obj.cost = parent.cost;

		tcart = cartesian (theta, [410 210]);
		pcart = cartesian (parent.theta, [410 210]);

                obj.cost = obj.cost + calculateDistance (tcart,pcart);
            end            
            obj.theta = theta;
        end
        function candidates = buildCandidates(obj, step)
            candidates(1) = Node([obj.theta(1) + step, obj.theta(2), obj.theta(3)], obj);
            candidates(2) = Node([obj.theta(1), obj.theta(2) + step, obj.theta(3)], obj);
            candidates(3) = Node([obj.theta(1), obj.theta(2), obj.theta(3) + step], obj);

            candidates(4) = Node([obj.theta(1) + step, obj.theta(2) + step,...
	    obj.theta(3)], obj);


            candidates(5) = Node([obj.theta(1) + step, obj.theta(2),...
	    obj.theta(3) + step], obj);


            candidates(6) = Node([obj.theta(1), obj.theta(2) + step,...
	    obj.theta(3) + step], obj);

%% sep

            candidates(7) = Node([obj.theta(1) - step, obj.theta(2) - step,...
	    obj.theta(3)], obj);


            candidates(8) = Node([obj.theta(1) - step, obj.theta(2),...
	    obj.theta(3) - step], obj);


            candidates(9) = Node([obj.theta(1), obj.theta(2) - step,...
	    obj.theta(3) - step], obj);


            candidates(10) = Node([obj.theta(1) - step, obj.theta(2), obj.theta(3)], obj);
            candidates(11) = Node([obj.theta(1), obj.theta(2) - step, obj.theta(3)], obj);
            candidates(12) = Node([obj.theta(1), obj.theta(2), obj.theta(3) - step], obj);

            candidates(13) = Node([obj.theta(1) + step, obj.theta(2) + step,...
	    obj.theta(3) + step], obj);

            candidates(14) = Node([obj.theta(1) - step, obj.theta(2) - step,...
	    obj.theta(3) - step], obj);
            
            % Pruning
            for k = 1:length(candidates)
                if candidates(k) == candidates(k).parent.parent
                 candidates(k) = [];
                 k = k - 1;
                end
            end
        end
       %function isvalid = validate(theta)
       %     j1 = theta(1)
       %     j2 = theta(2)
       %     j3 = theta(3)
       % end
    end
end
