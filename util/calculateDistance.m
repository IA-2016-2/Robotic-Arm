function distance = calculateDistance(A, B)
        distance = 0;
        for k = 1:length(A)
            distance = distance + power(A(k) - B(k), 2);
        end
        distance = sqrt(distance);
end