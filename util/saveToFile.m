function saveToFile(matriz, filename)
disp(['Saving file ', filename, '.txt'])
fileID = fopen(strcat(filename, '.txt'),'w');
fprintf(fileID,'%1.5f %2.5f %3.5f %5.5f %5.5f %6.5f\r\n', matriz); 
fclose(fileID);
disp(['File ', filename, '.txt saved'])
end