function plotER (reinforcement)
reinforcement = flipud (reinforcement);

for i = 1:length(reinforcement)
    if reinforcement(i, 2) < 1
          c = '-bo';
      else
          c = '-ro';
    end
      plot (i, reinforcement (i), c);
      hold on
end

plot (reinforcement (:, 1));
xlabel('Epoch');
ylabel('R');
end