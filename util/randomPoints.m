%% This function returns two random points on the workspace of the arm
% SYNTAX: randomPoints()
% OUTPUT: An array of coordinates x, y, z reachable by the arm, the first
% element of the array is the beginning, and the second element is the
% destination.
function randomPoints = randomPoints()

    % Gera o espa�o de trabalho do bra�o
    workspace = limits();
    
    % Gera o indice aleatorio do inicio
    index = ceil(rand * size(workspace,1));
    beginning = workspace(index,:);
    
    % Gera o indice aleatorio do destino
    index = ceil(rand * size(workspace,1));
    destination = workspace(index,:);
    
    randomPoints = [beginning, destination];
end