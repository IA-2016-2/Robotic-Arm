%% This function returns the arm workspace to serve as the basis for the algorithms
% The function uses limits based on the specifications of the arm
% SYNTAX: limits()
% OUTPUT: An giant array of coordinates x, y, z reachable by the arm
function limits = limits()

    % Limit: J2:±120º
    % Using ±100º for safety
    j2 = -1.7453:0.1:1.7453; % 100º = 1.7453rad
    j3 = j2;
    
    % Limit: J1:±160°
    % Using ±120º for safety
    j1 = -2.0944:0.1:2.0944;
    %%
    [theta1, theta2, theta3] = meshgrid(j1,j2,j3); %generate grid of theta1 and theta2
    limits = [theta1(:), theta2(:), theta3(:)];
%{
% Cartesian limits
arm1 = 210; % Size of arm1 according to the specifications
arm2 = 210; % Size of arm2 according to the specifications
x = arm1 * cos(theta1) + arm2 * cos(theta1 + theta2); % compute x coordinates
y = arm1 * sin(theta1) + arm2 * sin(theta1 + theta2); % compute y coordinates
z = arm2*sin(theta3); % compute z coordinates

limits = [x(:) y(:) z(:)]; %create x-y-z dataset
%}
%%
% Plot the max range of the arm graphically
%{
plot3(x(:),y(:),z(:));
axis equal
xlabel('X','fontsize',10)
ylabel('Y','fontsize',10)
ylabel('Z','fontsize',10)
%}
end