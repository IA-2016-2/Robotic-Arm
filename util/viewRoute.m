%% Takes route: matrix n by 3 and dest vector(3) as argument
%  and plots them cartesian(3)
function viewRoute (route, dest)
    len = length(route);
    start = route(1,1:3);
    pend  = route(len,1:3);
    %route = route(2:len,1:3);
    %len = length(route);
    plot3(route(1:len,1),route(1:len,2),route(1:len,3));
    hold on;
    scatter3(route(1:len,1),route(1:len,2),route(1:len,3),'filled','yellow');
    hold on;
    scatter3(start(1),start(2),start(3),'filled','blue');
    text(start(1),start(2),start(3), 'START', 'horizontal','left', 'vertical','bottom')
    hold on;
    scatter3(pend(1),pend(2),pend(3),'filled','red');
    text(pend(1),pend(2),pend(3), 'END', 'horizontal','left', 'vertical','bottom')
    hold on;
    scatter3(dest(1),dest(2),dest(3),'filled','green');
    text(dest(1),dest(2),dest(3), 'DEST', 'horizontal','left', 'vertical','bottom')
    view(3);
end