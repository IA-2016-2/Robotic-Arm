function matriz = readFromFile (filename)
file = strcat(filename, '.txt');

if exist(file, 'file')
    disp(['Reading file ', filename, '.txt'])
    fileID = fopen(file, 'r');
    formatSpec = '%f';
    vec = fscanf(fileID,formatSpec);
    matriz = reshape (vec, 6, [])';
    fclose(fileID);
    disp(['File ', filename, '.txt read'])
else
    disp(['File ', filename, '.txt does not exists'])
    matriz = [];
end
end