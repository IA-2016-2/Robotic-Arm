%% This function converts the robotic arm polar coordinates to cartesian
%  SYNTAX: cartesian(theta, linkSize)
%  WHERE: theta is a 1x3 matrix containing theta1 theta2 and theta3,
%  respectively. linkSize is a 1x2 matrix containing the lenghts of the
%  robotic arm links.
%  OUTPUT: A 1x3 matrix containing x, y and z, in order.
function p = cartesian(theta, linkSize)
    % 0 <= theta1 <= 2*pi
    % -pi/2 <= theta2 <= pi/2
    % -pi/2 <= theta3 <= pi/2
    
    % Calculating z and projection on XY plane, based on
    % https://www.mathworks.com/help/fuzzy/examples/modeling-inverse-kinematics-in-a-robotic-arm.html
    z = linkSize(1) * sin(theta(2)) + linkSize(2) * sin(theta(2) + theta(3));
    proj = linkSize(1) * cos(theta(2)) + linkSize(2) * cos(theta(2) + theta(3));
    
    % Using proj to get x and y
    x = proj * cos(theta(1));
    y = proj * sin(theta(1));
    
    p = [x, y, z];
end