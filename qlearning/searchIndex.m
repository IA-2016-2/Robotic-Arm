function index = searchIndex(angle, beginIndex, endIndex, reference)
	
	global order;
    order = [3, 1, 2];
    
    if ~exist('beginIndex', 'var')
                beginIndex = 1;
    end
    
    if ~exist('endIndex', 'var')
		endIndex = length(reference);
    end

    
    i = round((endIndex + beginIndex) / 2);
    if abs(beginIndex - endIndex) == 0
		disp(['Angle [', num2str(angle(1)), ', ' , num2str(angle(2)), ', ', num2str(angle(3)), '] not found in reference matrix']);
    else
        if isEquals(angle, reference(i, :), 3)
            index = i;
        elseif side(angle, reference(i, :), 3)
            index = searchIndex(angle, i, endIndex, reference);
        else
            index = searchIndex(angle, beginIndex, i, reference);
        end
    end
end

function equals = isEquals(ref, curr, k)
    global order;
    equals  = 1; 
    for i = (1:k)
        if ref(order(i)) ~= curr(order(i))
            equals = 0;
            break;
        end
    end      
end

function rightSide = side(ref, curr, k)
    global order;
    for i = (1:k) 
        if ref(order(i)) < curr(order(i))
            rightSide = 0;
            break;
        elseif ref(order(i)) > curr(order(i))
            rightSide = 1;
            break;
        end
    end
end