classdef QLearning
    properties
        startingAngles,
        tolerance,
        step,
        linkSize,
        q,
        j1,
        j2,
        j3
    end
    methods
        function obj = QLearning(startingAngles, linkSize, step, q)
            if ~exist('startingAngles', 'var')
                obj.startingAngles = [0, pi/2, -pi/2];  
            else
                obj.startingAngles = startingAngles;  
            end          
            
            if ~exist('linkSize', 'var')
                obj.linkSize = [210, 210];
            else
                obj.linkSize = linkSize;
            end
            
            if ~exist('step', 'var')
                obj.step = 0.1;
            else
                obj.step = step;
            end
            
            obj.tolerance = 25;
            %obj.reference = limits();
            obj.j1 = -2.0944:obj.step:2.0944;
            obj.j2 = -1.7453:obj.step:1.7453;
            obj.j3 = obj.j2;
                        
            if ~exist('q', 'var')
                % initializing Q(s,a) randomly
                obj.q = rand(length(obj.j1), length(obj.j2), length(obj.j3), 6)./1000;
            else
                obj.q = q;
            end
        end
        
        function route = train(obj, destination, epoch, maxRouteSize, learningRate, gama)
            % plot
            reinforcement = zeros (epoch, 2);
            
            if ~exist('maxRouteSize', 'var')
                maxRouteSize = 500;
            end
            
            if ~exist('epoch', 'var')
                epoch = 100;
            end
            
            if ~exist('learningRate', 'var')
                learningRate = 0.5;
            end
            
            if ~exist('gama', 'var')
                gama = 0.5;
            end
            
            angles = obj.startingAngles;
            
            % preallocating route
            route = zeros(maxRouteSize, 3);
            route(1, :) = [angles(1), angles(2), angles(3)];
            routeSize = 1;
            
            % repeat for each episode
            counter = 0;
            while counter < epoch
                numberOfIterations = 0;
                
                disp(['Now on epoch ', num2str(counter + 1)]);
                
                % repeat until we get to the final state or counter explodes
                while calculateDistance(cartesian(angles, obj.linkSize), destination) > obj.tolerance
                    
                    s = obj.anglesToIndex(angles);
                    
                    eg = rand;
                    if eg <= (0.2 * ((epoch - counter + 1)/epoch))
                        % e-guloso
                        a = round(5*rand) + 1;
                    else
                        % qlearning
                        % choose which action to make based on the current state
                        [~, a] = max(obj.q(s(1), s(2), s(3), :));
                    end
                    
                    % Doing the action
                    nextAngles = angles;
                    switch a
                        case 1
                            nextAngles(1) = angles(1) + obj.step;
                        case 2
                            nextAngles(2) = angles(2) + obj.step;
                        case 3
                            nextAngles(3) = angles(3) + obj.step;
                        case 4
                            nextAngles(1) = angles(1) - obj.step;
                        case 5
                            nextAngles(2) = angles(2) - obj.step;
                        case 6
                            nextAngles(3) = angles(3) - obj.step;
                    end
                    
                    % Working now with the next state
                    next = obj.anglesToIndex(nextAngles);
                    
                    % Treating out of reach problems
                    if any(next < 1) || ...
                            next(1) > length(obj.j1) || ...
                            next(2) > length(obj.j2) || ...
                            next(3) > length(obj.j3)
                        obj.q(s(1), s(2), s(3), a) = -Inf;
                        continue;
                    end
                  
                    % Saving route in last epoch
                    if (counter + 1) == epoch
                        routeSize = routeSize + 1;
                        route(routeSize, :) = [nextAngles(1), nextAngles(2), nextAngles(3)];
                    end
                    
                    % Estimating return based on the next state
                    nextDistance = calculateDistance(cartesian(nextAngles, obj.linkSize), destination);
                    r = ((0.7 * numberOfIterations) + (0.3 * nextDistance))/(nextDistance * numberOfIterations);
                    reinforcement (counter + 1, :) = [r; 0];
                    
                    % calculating Q(s, a) again
                    obj.q(s(1), s(2), s(3), a) = obj.q(s(1), s(2), s(3),a) ...
                        + learningRate * (r ...
                        + gama * max(obj.q(next(1), next(2), next(3), :)) ...
                        - obj.q(s(1), s(2), s(3),a));
                    
                    % Setting the next iteration
                    angles = nextAngles;
                    numberOfIterations = numberOfIterations + 1;
                       
                    % Avoiding the arm to go astray
                    if numberOfIterations > maxRouteSize
                        angles = obj.startingAngles;
                        if (counter + 1) == epoch
                            % In last epoch we retry
                            route = zeros(maxRouteSize, 3);
                            route(1, :) = [angles(1), angles(2), angles(3)];
                            routeSize = 1;
                            disp('[WW] Making new attempt');
                            continue;
                        else
                            % In ordinary epochs we pass
                            disp('    Epoch failed');
                            reinforcement (counter + 1, 2) = 5;
                            break;
                        end
                    end
                end
                
                angles = obj.startingAngles;
                counter = counter + 1;
            end
            
            % Removing preallocated rows not used
            for k = (routeSize + 1):maxRouteSize
                route(routeSize + 1, :) = [];
            end
            
            plotER(reinforcement);
        end
        
        function indexes = anglesToIndex(obj, angles)
            indexes(1) = round((angles(1) - obj.j1(1))/obj.step) + 1;
            indexes(2) = round((angles(2) - obj.j2(1))/obj.step) + 1;
            indexes(3) = round((angles(3) - obj.j3(1))/obj.step) + 1;
        end
    end
end