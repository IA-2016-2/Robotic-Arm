tic
clc

disp('--- QLearning Test ---');

disp('-> Initializing algorithm');
if (exist('matrixQ.mat', 'file'))
    load('matrixQ.mat');
    qlearning = QLearning([0, pi/2, -pi/2], [210, 210], 0.1, q);
else
    qlearning = QLearning([0, pi/2, -pi/2]);
end

disp('-> Training algorithm');
route = qlearning.train([0, 0, 420], 1000, 500);

% saving matrix q
q = qlearning.q;
save('matrixQ.mat', 'q', '-mat');

% Just displaying the results
disp('       Angles        =>        Coord');
for k = 1:length(route)
    coord = cartesian(route(k, :), [210, 210]);
    disp(['(', num2str(route(k, 1)), ', ', num2str(route(k, 2)), ', ', num2str(route(k, 3)), ')', ...
        ' | ', ...
        '(', num2str(coord(1)), ', ', num2str(coord(2)), ', ', num2str(coord(3)), ')']);
end

TimeSpent = toc;
disp (['Tempo gasto: ', num2str(toc), ' segundos.'])