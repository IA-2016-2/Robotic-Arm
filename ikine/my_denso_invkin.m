function theta123 = my_denso_invkin(x4, y4, z4)

%% Set arms lengths
l1 = 210;
l2 = sqrt(75.^2 + 210.^2);

%% Compute theta1
t1 = atan2(y4, x4);

A_1 = [ cos(t1), 0,  sin(t1),   0; ...
        sin(t1), 0, -cos(t1),   0; ...
        0, 1,       0, 125; ...
        0, 0,       0,   1];

%% Compute the origin of the 4th frame wrt to frame {1}
P41 = A_1 \ [x4; y4; z4; 1];
p4x1 = P41(1);
p4y1 = P41(2);
p4z1 = P41(3);

%% Compute the cosine of theta3
c3 = (p4x1.^2 + p4y1.^2 + p4z1.^2  - l1.^2 - l2.^2)./ ...
    (2 * l1 * l2);

s3 = - sqrt(1 - c3.^2); % Elbow up
alpha = atan2(210,75);
t3 = atan2(s3, c3) + alpha;

phi = atan2( p4y1 , sqrt(x4.^2 + y4.^2) );
beta = atan2 ( ...
    sin( -t3 + alpha) * sqrt(210.^2 + 75.^2) , ...
    210 + cos(-t3 + alpha) * sqrt(210.^2 + 75.^2));

t2 = -pi/2 + phi + beta;

theta123 = [t1, t2, t3];