%% Set path
nturns = 5;

t = 0:1e2;
phi = nturns * t/max(t) * 2 * pi;
r = 10 + 40 * t/max(t);
z = 150 + 160 * t/max(t);

x = 200 + r .* cos(phi);
y = r .* sin(phi);


coord = cartesian(route(k, :), linkSize);
for k = 1:length(route)
    coord = [coord; cartesian(route(k, :), linkSize)];
end

x = coord(:,1);
y = coord(:,2);
z = coord(:,3);


hold on
plot3(x,y,z)
view(70,40);
axis tight equal

%% Iterate over path
for i = 1:numel(x)
    
    q0 = my_denso_invkin(x(i), y(i), z(i));

    q = [...
        q0(1), ...
        q0(2), ...
        q0(3)-pi/2, ...
        0, ...
        - ( pi/2 + q0(2) + q0(3) ), ...
        0];
    cla;
    plotDENSOframes(q);
    view(130, 30);
    axis tight
    pause(.01);
    
end
hold on
plot3(x,y,z)
rotate3d on