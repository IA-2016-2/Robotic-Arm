function plotframe(T, varargin)
%PLOTFRAME Plot xyz frame in 3D space
%   PLOTFRAME(T, varargin) Plot an XYZ frame defined by T in 3D space. T
%   must be a 4x4 homogeneous matrix specifying the frame with respect to
%   the base frame, i. e., like:
%
%   T = [...
%       r11, r12, r13, p1; ...
%       r21, r22, r23, p2; ...
%       r31, r32, r33, p3; ...
%       0,     0,   0,  1];
%
%   PLOTFRAME(T, 'currAxes', GCA) passes a handle to current axes. Useful
%   for plotting more than one frame in the same figure.
%
%   PLOTFRAME(T, 'putText', <TEXT>) puts the string TEXT in the origin of
%   frame T, to identify the frame with an ID, e.g., '\{1\}'.
%
%   PLOTFRAME(T, 'vectorScale', VSCALE) specifies the scale of each frame
%   arrow with respect to the globla coordinate frame. Useful to plot
%   frames in different scales.

%%
p = inputParser;

defaultVectorScale = 1.0;
defaultText = '';
% defaultT = eye(4);

% Handle to existing axes
addRequired(p, 'T', @(x) size(T, 1) == 4 && size(T,2) == 4);
addParameter(p, 'vectorScale', defaultVectorScale, @(x) x > 0);
addParameter(p, 'currAxes', gca, @ishandle);
addParameter(p, 'putText', defaultText, @ischar);

parse(p, T, varargin{:});

%%
scale = p.Results.vectorScale;
currAxis = p.Results.currAxes;
T = p.Results.T;
label = p.Results.putText;

%%
% Plot axis 'X'
quiver3(currAxis, ...
    T(1,4), T(2,4), T(3,4), ... % Position
    scale*T(1,1), scale*T(2,1), scale*T(3,1), ... %
    'color', 'r', ...
    'lineWidth', 2);

% Plot axis 'Y'
quiver3(currAxis, ...
    T(1,4), T(2,4), T(3,4), ...
    scale*T(1,2), scale*T(2,2), scale*T(3,2), ...
    'color', 'g', ...
    'lineWidth', 2);

% Plot axis 'Z'
quiver3(currAxis, ...
    T(1,4), T(2,4), T(3,4), ...
    scale*T(1,3), scale*T(2,3), scale*T(3,3), ...
    'color', 'b', ...
    'lineWidth', 2);

%%
text(T(1,4), T(2,4), T(3,4), label);