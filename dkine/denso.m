function varargout = denso(varargin)
% DENSO MATLAB code for denso.fig
%      DENSO, by itself, creates a new DENSO or raises the existing
%      singleton*.
%
%      H = DENSO returns the handle to a new DENSO or the handle to
%      the existing singleton*.
%
%      DENSO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DENSO.M with the given input arguments.
%
%      DENSO('Property','Value',...) creates a new DENSO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before denso_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to denso_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help denso

% Last Modified by GUIDE v2.5 26-Nov-2016 16:29:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @denso_OpeningFcn, ...
    'gui_OutputFcn',  @denso_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before denso is made visible.
function denso_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to denso (see VARARGIN)

% Choose default command line output for denso
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes denso wait for user response (see UIRESUME)
% uiwait(handles.figure1);

hold on
view(135, 45);
global qVec;
qVec = [0 0 -pi/2 0 0 0]';
% thetaVec = qVec + [0 pi/2 -pi/2 0 0 0]';
% disp(qVec);

plotDENSOframes(qVec);

axis equal
set(gca, 'PlotBoxAspectRatio', [1 1 1])
xlim([-700, 700]);
ylim([-700, 700]);
zlim([-300, 700]);

% Ax = findall(0,'type','axes');
% axis(Ax,[-5 5 -5 5 -5 5]);

xlabel('X'), ylabel('Y'), zlabel('Z');
grid on;



% --- Outputs from this function are returned to the command line.
function varargout = denso_OutputFcn(~, ~, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, ~, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% cla;
valDeg = get(hObject,'Value');
valRad = valDeg * pi/180;
global qVec;
qVec(1) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.theta_1_text, 'String', sprintf('%.2f',valDeg));

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, ~, ~)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, ~, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
valDeg = get(hObject,'Value');
valRad = valDeg * pi/180;
global qVec;
qVec(2) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.theta_2_text, 'String', sprintf('%.2f',valDeg));

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, ~, ~)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider3_Callback(hObject, ~, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
valDeg = get(hObject,'Value');
valRad = valDeg * pi/180;
global qVec;
qVec(3) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.theta_3_text, 'String', sprintf('%.2f',valDeg));

% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, ~, ~)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider4_Callback(hObject, ~, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
valDeg = get(hObject,'Value');
valRad = valDeg * pi/180;
global qVec;
qVec(4) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.theta_4_text, 'String', sprintf('%.2f',valDeg));

% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, ~, ~)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider5_Callback(hObject, ~, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
valDeg = get(hObject,'Value');
valRad = valDeg * pi/180;
global qVec;
qVec(5) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.theta_5_text, 'String', sprintf('%.2f',valDeg));

% --- Executes during object creation, after setting all properties.
function slider5_CreateFcn(hObject, ~, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider6_Callback(hObject, ~, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
valDeg = get(hObject,'Value');
valRad = valDeg * pi/180;
global qVec;
qVec(6) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.theta_6_text, 'String', sprintf('%.2f',valDeg));

% --- Executes during object creation, after setting all properties.
function slider6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function theta_1_text_Callback(hObject, ~, handles)
% hObject    handle to theta_1_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of theta_1_text as text
%        str2double(get(hObject,'String')) returns contents of theta_1_text as a double
valDeg = str2double(get(hObject,'String'));
valRad = valDeg * pi/180;
global qVec;
qVec(1) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.slider1, 'value', valDeg);

% --- Executes during object creation, after setting all properties.
function theta_1_text_CreateFcn(hObject, ~, ~)
% hObject    handle to theta_1_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function theta_2_text_Callback(hObject, ~, handles)
% hObject    handle to theta_2_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of theta_2_text as text
%        str2double(get(hObject,'String')) returns contents of theta_2_text as a double
valDeg = str2double(get(hObject,'String'));
valRad = valDeg * pi/180;
global qVec;
qVec(2) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.slider2, 'value', valDeg);

% --- Executes during object creation, after setting all properties.
function theta_2_text_CreateFcn(hObject, ~, ~)
% hObject    handle to theta_2_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function theta_3_text_Callback(hObject, ~, handles)
% hObject    handle to theta_3_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of theta_3_text as text
%        str2double(get(hObject,'String')) returns contents of theta_3_text as a double
valDeg = str2double(get(hObject,'String'));
valRad = valDeg * pi/180;
global qVec;
qVec(3) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.slider3, 'value', valDeg);

% --- Executes during object creation, after setting all properties.
function theta_3_text_CreateFcn(hObject, ~, ~)
% hObject    handle to theta_3_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function theta_4_text_Callback(hObject, ~, handles)
% hObject    handle to theta_4_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of theta_4_text as text
%        str2double(get(hObject,'String')) returns contents of theta_4_text as a double
valDeg = str2double(get(hObject,'String'));
valRad = valDeg * pi/180;
global qVec;
qVec(4) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.slider4, 'value', valDeg);

% --- Executes during object creation, after setting all properties.
function theta_4_text_CreateFcn(hObject, ~, ~)
% hObject    handle to theta_4_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function theta_5_text_Callback(hObject, ~, handles)
% hObject    handle to theta_5_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of theta_5_text as text
%        str2double(get(hObject,'String')) returns contents of theta_5_text as a double
valDeg = str2double(get(hObject,'String'));
valRad = valDeg * pi/180;
global qVec;
qVec(5) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.slider5, 'value', valDeg);

% --- Executes during object creation, after setting all properties.
function theta_5_text_CreateFcn(hObject, ~, ~)
% hObject    handle to theta_5_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function theta_6_text_Callback(hObject, ~, handles)
% hObject    handle to theta_6_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of theta_6_text as text
%        str2double(get(hObject,'String')) returns contents of theta_6_text as a double
valDeg = str2double(get(hObject,'String'));
valRad = valDeg * pi/180;
global qVec;
qVec(6) = valRad;
% disp(qVec);
cla
plotDENSOframes(qVec);
set(handles.slider6, 'value', valDeg);

% --- Executes during object creation, after setting all properties.
function theta_6_text_CreateFcn(hObject, ~, ~)
% hObject    handle to theta_6_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
