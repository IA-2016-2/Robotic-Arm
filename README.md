## Projeto final de IA (Inteligência Artificial) de Engenharia da Computação (IC UFAL) no semestre letivo 2016.2.

# Passos usados no trabalho com o GitLab:

- A nova branch deve ser criada através da interface do GitLab(browser), indo em Issues > 'Nome da Issue'
- Clicar em 'New Branch'
- A label deve ser alterada em Issues > Board para a label de status da mesma
- Clonar a branch de trabalho atual, usando o comando git clone -b "branch-name" --single-branch https://gitlab.com/IA-2016-2/Robotic-Arm.git
- Ao adicionar mudanças no commit, é recomendado o usado do comando "git add ."
- Ao "commitar" sua branch de trabalho, recomenda-se o uso do comando "git commit -s" ou "git commit -s -m "mensagem de commit"
- É recomendado usar tag "WIP: 'nome-do-branch' - ' realizei algo '" nas mensagens de commit.

WIP significa Work in Progress, logo a tag deve ser removida ao finalizar a branch no último commit da mesma.

## Acesse a [Wiki](https://gitlab.com/IA-2016-2/Robotic-Arm/wikis/home) do projeto para ver os tutoriais disponíveis.

## OBS: Para simular é preciso rodar o dkine antes do ikine, pois o dkine gera o DENSO.

## Good Luck, Have Fun!